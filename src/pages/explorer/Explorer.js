import React, { useEffect, lazy, Suspense } from "react";
import { Tabs, Tab, Button } from "@nextui-org/react";

import { useTabsStore } from "../../store/tabs";
import { useParams } from "react-router-dom";

const Sidebar = lazy(() => import("./components/Sidebar/Sidebar"));
const Navigation = lazy(() => import("../../components/Navbar"));
const QueryEditor = lazy(() => import("./components/QueryEditor/QueryEditor"));
const QueryResults = lazy(() =>
  import("./components/QueryResults/QueryResults")
);


const Explorer = () => {
  const { newTabs, activeTab, addTab, addSavedTab, updateActiveTab } = useTabsStore();
  const params = useParams();

  useEffect(() => {
    if (params.id) {
      addSavedTab(params.id);
    }
  }, [params.id]);

  return (
    <Suspense fallback={<p className="h-full flex justify-center items-center">Loading</p>}>
      <div className="bg-gray-200 h-full">
        <Navigation />
        <div className="flex h-full">
          <Sidebar />
          <div className="flex-auto p-2">
            <Button radius="none" onClick={() => addTab()}>
              +
            </Button>
            <Tabs
              aria-label="Options"
              radius="none"
              size="lg"
              variant="underlined"
              classNames={{
                base: "max-w-[90%] overflow-scroll",
              }}
              selectedKey={activeTab}
              onSelectionChange={updateActiveTab}
            >
              {Object.values(newTabs).map((tab) => (
                <Tab key={tab.id} title={tab.name} className="h-full">
                  <QueryEditor tab={tab} />
                  <QueryResults tab={tab} />
                </Tab>
              ))}
            </Tabs>
          </div>
        </div>
      </div>
    </Suspense>
  );
};

export default Explorer;
