export const fetchQueryResult = (query = "", id = 1, page = 1, rowsPerPage = 10) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const fixedQueryResults = [
        "customers",
        "categories",
        "products",
        "employees",
      ];
      if (id && id !== 5) {
        const data = require(`../../../../../schemas/${
          fixedQueryResults[id - 1]
        }.json`);
        resolve({
          total: data.length,
          data: data.slice((page - 1) * rowsPerPage, (page - 1) * rowsPerPage + rowsPerPage),
        });
      } else if (id === 5) {
        const products = require(`../../../../../schemas/${fixedQueryResults[2]}.json`);
        resolve([
          {
            total: 1,
            data: products[0],
          },
        ]);
      } else {
        const data = require(`../../../../../schemas/${
          fixedQueryResults[3]
        }.json`);
        resolve({
          total: data.length,
          data: data.slice(
            (page - 1) * rowsPerPage,
            (page - 1) * rowsPerPage + rowsPerPage
          ),
        });
      }
    }, 1000);
  });
};
