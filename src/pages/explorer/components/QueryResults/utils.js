export const getColumnsFromResult = (result) => {
  return Object.keys(result).map((key) => ({
    key,
    label: key
  }));
}