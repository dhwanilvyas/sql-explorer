import React, { Fragment, useState, useMemo, useRef, useEffect } from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Spacer,
  Skeleton,
  Image,
  Pagination,
  Spinner,
  Input,
  getKeyValue,
} from "@nextui-org/react";
import { fetchQueryResult } from "./service";
import SomethingWentWrongImage from "../../../../assets/somethingwentwrong.png";
import { getColumnsFromResult } from "./utils";
import { useTabsStore } from "../../../../store/tabs";
import { FaSearch } from "react-icons/fa";

const QueryResults = ({ tab }) => {
  const [loadingMore, setLoadingMore] = useState("idle");
  const [error, setError] = useState(null);
  const { runQueryComplete, updateTab } = useTabsStore();
  const shouldLoadMore = useRef(false);

  useEffect(() => {
    if (tab.isRunning) {
      if (tab.savedId === "2") {
        setError(true);
        runQueryComplete(tab.id, [], 0);
      } else {
        if (!shouldLoadMore.current) shouldLoadMore.current = true;
        fetchResults();
      }
    }
  }, [tab.isRunning]);

  useEffect(() => {
    if (shouldLoadMore.current) {
      fetchResults();
    }
  }, [tab.page, tab.rowsPerPage]);

  const fetchResults = async () => {
    const queryResults = await fetchQueryResult(
      tab.savedQuery,
      tab.savedId,
      tab.page,
      tab.rowsPerPage
    );

    setLoadingMore("idle");
    runQueryComplete(tab.id, queryResults.data, queryResults.total);
  };

  const updatePage = (val) => {
    setLoadingMore("loadingMore");
    updateTab("page", val);
  };

  const columns = useMemo(() => {
    if (tab.results && tab.results.length) {
      return getColumnsFromResult(tab.results[0]);
    }

    return [];
  }, [tab.results]);

  const topContent = (
    <div className="flex justify-between items-center">
      <div className="flex gap-3 flex-1">
        <label className="flex items-center text-small">
          Search by:
          <select
            className="bg-transparent outline-none text-small"
            onChange={(event) =>
              updateTab("filterByColumn", event.target.value)
            }
            defaultValue=""
            value={tab.filterByColumn}
          >
            <option value="" disabled>
              column
            </option>
            {columns.map((column) => (
              <option key={column.key} value={column.key}>
                {column.label}
              </option>
            ))}
          </select>
        </label>
        <Input
          isClearable
          className="w-1/3"
          placeholder="Search here"
          disabled={!tab.filterByColumn}
          startContent={<FaSearch />}
          value={tab.searchInput}
          onClear={() => updateTab("filterByColumn", "")}
          onValueChange={(val) => updateTab("searchInput", val)}
        />
      </div>
      <div className="flex gap-3">
        <span className="text-small">Total rows: {tab.totalResults}</span>
        <label className="flex items-center text-small">
          Rows per page:
          <select
            className="bg-transparent outline-none text-small"
            onChange={(event) => updateTab("rowsPerPage", event.target.value)}
          >
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
          </select>
        </label>
        <label className="flex items-center text-small">
          Download as:
          <select
            className="bg-transparent outline-none text-small"
            onChange={(e) =>
              alert(`This should download data as ${e.target.value}`)
            }
          >
            <option value="json">JSON</option>
            <option value="csv">CSV</option>
          </select>
        </label>
      </div>
    </div>
  );

  const bottomContent = (
    <div className="flex justify-center">
      <Pagination
        isCompact
        showControls
        showShadow
        color="secondary"
        page={tab.page}
        total={Math.floor(tab.totalResults / tab.rowsPerPage)}
        onChange={updatePage}
      />
    </div>
  );

  const filteredResults = useMemo(() => {
    if (!tab.filterByColumn || !tab.searchInput) {
      return tab.results;
    }

    return tab.results.filter(result => result[tab.filterByColumn].toString().toLowerCase().includes(tab.searchInput.toString().toLowerCase()));
  }, [tab.filterByColumn, tab.searchInput, tab.results]);

  if (tab.isRunning) {
    const loaders = Array.from(Array(3).keys());

    return (
      <div className="h-1/2 mt-2">
        {loaders.map((loader) => (
          <Fragment key={loader}>
            <Skeleton className="w-4/5 ml-2 mb-4">
              <div className="h-3 w-full bg-default-200"></div>
            </Skeleton>
            <Spacer x={4} />
          </Fragment>
        ))}
      </div>
    );
  }

  if (error) {
    return (
      <div className="flex flex-col items-center">
        <Image src={SomethingWentWrongImage} width={150} />
        <p>Something went wrong with the query.</p>
      </div>
    );
  }

  if (!tab.results.length) {
    return (
      <div className="flex justify-center items-center">
        <p>"Run Query" will show the results here.</p>
      </div>
    );
  }

  return (
    <div className="w-full h-3/4">
      <Table
        aria-label="Query results"
        radius="none"
        isHeaderSticky
        topContent={topContent}
        bottomContent={bottomContent}
        classNames={{
          base: "h-4/5 w-full overflow-scroll",
          table: "h-4/5 w-full",
        }}
        emptyContent={<p>No results found</p>}
      >
        <TableHeader columns={columns}>
          {(column) => (
            <TableColumn key={column.key}>{column.label}</TableColumn>
          )}
        </TableHeader>
        <TableBody
          loadingContent={<Spinner />}
          loadingState={loadingMore}
          items={filteredResults}
        >
          {(item) => (
            <TableRow key={item[columns[0].key]}>
              {(columnKey) => (
                <TableCell>{getKeyValue(item, columnKey)}</TableCell>
              )}
            </TableRow>
          )}
        </TableBody>
      </Table>
    </div>
  );
};

export default QueryResults;
