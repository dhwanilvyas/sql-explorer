import React, { useState } from "react";
import { Button, Popover, PopoverContent, PopoverTrigger } from "@nextui-org/react";
import CodeEditor from "@uiw/react-textarea-code-editor";

import { useTabsStore } from "../../../../store/tabs";
import { useQueriesStore } from "../../../../store/queries";
import { FaArrowRight, FaSave, FaShare } from "react-icons/fa";
import { SlClose } from "react-icons/sl";
import { useParams } from "react-router-dom";

const QueryEditor = ({ tab }) => {
  const [savingQuery, setSaving] = useState(false);
  const { saveQuery, updateQuery } = useQueriesStore();
  const { updateTab, closeActiveTab, runQuery } = useTabsStore();
  const params = useParams();

  const onSaveQueryClick = async () => {
    setSaving(true);
    if (tab.isSaved) {
      await updateQuery(tab.savedQuery, tab.savedId);
    } else {
      await saveQuery(tab.savedQuery);
    }
    updateTab("name", tab.savedQuery);
    setSaving(false);
  };

  return (
    <div className="h-1/4 mb-2">
      <div className="h-full flex justify-end gap-2 mb-2">
        <CodeEditor
          value={tab.savedQuery}
          placeholder="Enter your query here"
          language="sql"
          className="flex-1"
          onChange={(e) => updateTab("savedQuery", e.target.value)}
          padding={15}
          style={{
            fontSize: 16,
            backgroundColor: "#ffffff",
            overflow: "auto",
            fontFamily:
              "ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace",
          }}
        />
        <div className="flex flex-col gap-2">
          <Button
            size="md"
            color="primary"
            radius="none"
            onClick={runQuery}
            isLoading={tab.isRunning}
            isDisabled={!tab.savedQuery}
            endContent={<FaArrowRight />}
          >
            Run query
          </Button>
          <Button
            size="md"
            color="secondary"
            radius="none"
            onClick={onSaveQueryClick}
            isLoading={savingQuery}
            isDisabled={!tab.savedQuery}
            endContent={<FaSave />}
          >
            Save query
          </Button>
          {tab.isSaved ? (
            <Popover placement="right">
              <PopoverTrigger>
                <Button
                  size="md"
                  color="secondary"
                  radius="none"
                  endContent={<FaShare />}
                >
                  Share Query
                </Button>
              </PopoverTrigger>
              <PopoverContent>
                <div className="px-1 py-2">
                  <div className="text-small font-bold">
                    Share the below link
                  </div>
                  <div className="text-tiny">{`${window.location.href}/${tab.savedId || ''}`}</div>
                </div>
              </PopoverContent>
            </Popover>
          ) : null}
          <Button
            size="md"
            color="danger"
            radius="none"
            onClick={closeActiveTab}
            endContent={<SlClose />}
          >
            Close Tab
          </Button>
        </div>
      </div>
    </div>
  );
};

export default QueryEditor;
