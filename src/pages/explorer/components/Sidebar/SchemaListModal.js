import React, { Fragment, useState, useEffect } from "react";
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  Spacer,
  Skeleton,
  AccordionItem,
  Accordion,
} from "@nextui-org/react";
import { getSchemaColumns, getSchemaList } from "../../../../services/schema";

function SchemaListModal({ isOpen, onClose }) {
  const [loadingSchemas, setLoading] = useState(true);
  const [schemaList, setSchemaList] = useState([]);

  useEffect(() => {
    const fetchSchemas = async () => {
      const resSchemas = await getSchemaList();
      setSchemaList(resSchemas);
      setLoading(false);
    };
    fetchSchemas();
  }, []);

  if (!isOpen) return null;

  const renderModalContent = () => {
    if (loadingSchemas) {
      const loaders = Array.from(Array(3).keys());

      return (
        <>
          {loaders.map((loader) => (
            <Fragment key={loader}>
              <Skeleton className="w-4/5 ml-2 mb-4">
                <div className="h-3 w-full bg-default-200"></div>
              </Skeleton>
              <Spacer x={4} />
            </Fragment>
          ))}
        </>
      );
    }

    return (
      <>
        {schemaList.map((schema) => (
          <Schema key={schema} schema={schema} />
        ))}
      </>
    );
  }

  return (
    <>
      <Modal isOpen={isOpen} onOpenChange={onClose}>
        <ModalContent>
          <>
            <ModalHeader className="flex flex-col gap-1">
              Schemas Available
            </ModalHeader>
            <ModalBody>{renderModalContent()}</ModalBody>
          </>
        </ModalContent>
      </Modal>
    </>
  );
}

const Schema = ({ schema }) => {
  const [selectedKey, setSelectedKey] = useState(new Set([]));
  const [loadingColumns, setLoadingColumns] = useState(false);
  const [columns, setColumns] = useState([]);

  const loadColumns = async () => {
    setLoadingColumns(true);
    const responseColumns = await getSchemaColumns(schema);
    setColumns(responseColumns);
    setLoadingColumns(false);
  };

  const onSelectionChange = () => {
    if (selectedKey.has(schema)) {
      setSelectedKey(new Set([]));
    } else {
      setSelectedKey(new Set([schema]));
      loadColumns();
    }
  };

  const renderColumns = () => {
    return (
      <ul>
        {columns.map((column) => (
          <li key={column} className="ml-4 mb-2">
            {column}
          </li>
        ))}
      </ul>
    );
  };

  const renderLoader = () => {
    const loaders = Array.from(Array(5).keys());

    return (
      <>
        {loaders.map((loader) => (
          <Fragment key={loader}>
            <Skeleton className="w-3/5">
              <div className="h-3 w-3/5 bg-default-200"></div>
            </Skeleton>
            <Spacer x={4} />
          </Fragment>
        ))}
      </>
    );
  };

  return (
    <Accordion
      aria-label={schema}
      isCompact
      selectedKeys={selectedKey}
      onSelectionChange={onSelectionChange}
    >
      <AccordionItem key={schema} aria-label={schema} title={schema}>
        {loadingColumns ? renderLoader() : renderColumns()}
      </AccordionItem>
    </Accordion>
  );
};

export default React.memo(SchemaListModal);