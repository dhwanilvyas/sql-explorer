import React, { useState } from "react";
import { Tooltip, Button } from "@nextui-org/react";
import { ErrorBoundary } from "react-error-boundary";

import { FaDatabase, FaList } from "react-icons/fa";
import SavedQueriesModal from "./SavedQueriesModal";
import SchemaListModal from "./SchemaListModal";

function Sidebar() {
  const [currentOpenModal, setModal] = useState(null);
console.log('rerender');
  const onClose = React.useCallback(() => {
    setModal(null)
  }, []);

  return (
    <ErrorBoundary fallback={<div>Something went wrong</div>}>
      <div className="bg-white flex flex-col w-[80px]">
        <Tooltip showArrow placement="right" content="Saved Queries">
          <Button
            onPress={() => setModal("savedQueries")}
            radius="none"
            className="w-full border-y-2 border-solid border-slate-200"
            isIconOnly
            size="lg"
            variant="light"
            aria-label="Saved Queries"
          >
            <FaDatabase />
          </Button>
        </Tooltip>
        <Tooltip showArrow placement="right" content="Schemas">
          <Button
            onPress={() => setModal("schemas")}
            radius="none"
            className="w-full"
            isIconOnly
            size="lg"
            variant="light"
            aria-label="Schemas"
          >
            <FaList />
          </Button>
        </Tooltip>
        <SavedQueriesModal
          isOpen={currentOpenModal === "savedQueries"}
          onClose={onClose}
        />
        <SchemaListModal
          isOpen={currentOpenModal === "schemas"}
          onClose={onClose}
        />
      </div>
    </ErrorBoundary>
  );
}

export default React.memo(Sidebar);
