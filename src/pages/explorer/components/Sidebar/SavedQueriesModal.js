import React, { Fragment, useState, useEffect } from "react";
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  Listbox,
  ListboxItem,
  Skeleton,
  Spacer,
} from "@nextui-org/react";
import { SlArrowRight, SlTrash } from "react-icons/sl";

import { useQueriesStore } from "../../../../store/queries";
import { useTabsStore } from "../../../../store/tabs";
import { getSavedQueries } from "../../../../services/savedQueriesService";

function SavedQueriesModal({ isOpen, onClose }) {
  const [isDeleting, setDeleting] = useState(false);
  const [loading, setLoading] = useState(false);
  const {
    setSavedQueries,
    savedQueries,
    deleteSavedQuery,
    deleteAllSavedQueries,
  } = useQueriesStore();
  const closeSavedTab = useTabsStore(state => state.closeSavedTab);
  const addSavedTab = useTabsStore(state => state.addSavedTab);

  useEffect(() => {
    setLoading(true);
    getSavedQueries()
      .then(res => {
        setSavedQueries(res);
        setLoading(false);
      })
  }, []);

  const deleteSavedQueries = async () => {
    setDeleting(true);
    await deleteAllSavedQueries();
    setDeleting(false);
    onClose();
  };

  const onItemClick = async (e, id) => {
    if (e.target.id === "delete-icon") {
      deleteSavedQuery(id);
      closeSavedTab(id);
    } else {
      addSavedTab(id);
      onClose();
    }
  };

  const getDescription = (query) => {
    if (query === "SELECT * FROM CATEGORIES;") {
      return "This query will error out when run";
    }

    if (query === "SELECT * FROM PRODUCTS;") {
      return "This has huge columns & rows";
    }

    return null;
  };

  if (!isOpen) {
    return null;
  }

  const renderModalContent = () => {
    if (loading) {
      const loaders = Array.from(Array(3).keys());

      return (
        <>
          {loaders.map((loader) => (
            <Fragment key={loader}>
              <Skeleton className="w-4/5 ml-2 mb-4">
                <div className="h-3 w-full bg-default-200"></div>
              </Skeleton>
              <Spacer x={4} />
            </Fragment>
          ))}
        </>
      );
    }

    if (!savedQueries?.length) {
      return (
        <div>
          <p className="text-center">Saved queries show up here</p>
        </div>
      );
    }

    return (
      <Listbox aria-label="Actions" disabledKeys={isDeleting ? ["delete"] : []}>
        {savedQueries.map((query, index) => (
          <ListboxItem
            key={query.id}
            onClick={(e) => onItemClick(e, query.id)}
            description={getDescription(query.query)}
            endContent={
              <>
                <SlTrash id="delete-icon" className="text-danger" />
                <SlArrowRight />
              </>
            }
          >
            {query.query}
          </ListboxItem>
        ))}
        <ListboxItem
          key="delete"
          className="text-danger"
          color="danger"
          onClick={deleteSavedQueries}
        >
          Delete all queries
        </ListboxItem>
      </Listbox>
    );
  };

  return (
    <>
      <Modal isOpen={isOpen} onOpenChange={onClose}>
        <ModalContent>
          <ModalHeader className="flex flex-col gap-1">
            Saved Queries
          </ModalHeader>
          <ModalBody>{renderModalContent()}</ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}

export default React.memo(SavedQueriesModal);