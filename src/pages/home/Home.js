import {
  Link,
} from "@nextui-org/react";

const Home = () => {
  return (
    <div className="h-full flex flex-col justify-center items-center">
      <h1 className="text-xl">Welcome to Data Explorer</h1>
      <Link href="/explorer">Go to explorer</Link>
    </div>
  );
};

export default Home;
