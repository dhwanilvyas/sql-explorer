import { create } from "zustand";
import { immer } from "zustand/middleware/immer";

import {
  deleteSavedQueries,
  deleteSavedQuery,
  saveQuery,
  updateQuery
} from "../services/savedQueriesService";

export const useQueriesStore = create(immer((set) => ({
  savedQueries: [],
  setSavedQueries: (queries) => {
    set((state) => {
      state.savedQueries = queries;
    })
  },
  saveQuery: async (query) => {
    const id = await saveQuery(query);
    set((state) => ({
      savedQueries: [...state.savedQueries, { id, query }],
    }));
    return id;
  },
  updateQuery: async (query, queryId) => {
    await updateQuery(query, queryId);

    set((state) => ({
      savedQueries: state.savedQueries.map((q) => {
        if (q.id === queryId) {
          return {
            id: queryId,
            query,
          };
        }

        return q;
      }),
    }));
  },
  deleteSavedQuery: async (id) => {
    await deleteSavedQuery(id);
    set((state) => ({
      savedQueries: state.savedQueries.filter((query) => query.id !== id),
    }));
  },
  deleteAllSavedQueries: async () => {
    await deleteSavedQueries();
    set((state) => {
      state.savedQueries = [];
    });
  },
})));
