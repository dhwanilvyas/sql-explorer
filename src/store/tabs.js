import { create } from "zustand";
import { immer } from "zustand/middleware/immer";
import { getQuery } from "../services/savedQueriesService";

const initialTabValue = {
  id: "1",
  name: "Untitled 1",
  savedQuery: "",
  isSaved: false,
  isRunning: false,
  results: [],
  page: 1,
  totalResults: 0,
  rowsPerPage: 10,
  filterByColumn: "",
  searchInput: "",
};

export const useTabsStore = create(
  immer((set) => ({
    idCounter: 1,
    newTabs: {
      1: {
        ...initialTabValue,
      },
    },
    activeTab: "1",
    addTab: () => {
      set((state) => {
        const idCounter = state.idCounter + 1;
        state.idCounter = idCounter;
        state.activeTab = idCounter.toString();
        state.newTabs[idCounter] = {
          ...initialTabValue,
          id: idCounter.toString(),
          name: `Untitled ${idCounter}`,
        };
      });
    },
    addSavedTab: async (id) => {
      const query = await getQuery(id);
      set((state) => {
        if (!query || Object.values(state.newTabs).find((tab) => tab.savedId === id)) {
          return state;
        }

        const idCounter = state.idCounter + 1;
        state.idCounter = idCounter;
        state.newTabs[idCounter] = {
          ...initialTabValue,
          id: idCounter.toString(),
          name: query,
          savedId: id,
          savedQuery: query,
          isSaved: true,
        };
        state.activeTab = idCounter.toString();
      });
    },
    closeSavedTab: (id) => {
      set((state) => {
        const tabToRemove = Object.values(state.newTabs).find(
          (stateTab) => stateTab.savedId === id
        )?.id;
        delete state.newTabs[tabToRemove];
        if (state.activeTab === tabToRemove) {
          const ids = Object.keys(state.newTabs);
          state.activeTab = ids[ids.length - 1];
        }
      });
    },
    clostAllSavedTabs: () => {
      set((state) => {
        const idsToDelete = Object.keys(state.newTabs)
          .map((key) => {
            if (state.newTabs[key].isSaved) {
              return key;
            }

            return undefined;
          })
          .filter(Boolean);

        idsToDelete.forEach((idToDelete) => {
          delete state.newTabs[idToDelete];
        });

        if (!state.newTabs.hasOwnProperty(state.activeTab)) {
          const ids = Object.keys(state.newTabs);
          state.activeTab = ids[ids.length - 1];
        }
      });
    },
    closeActiveTab: () => {
      set((state) => {
        if (Object.values(state.newTabs).length > 1) {
          delete state.newTabs[state.activeTab];

          const objValues = Object.values(state.newTabs);
          const lastTab = objValues.length - 1;
          state.activeTab = objValues[lastTab].id;
        }

        return state;
      });
    },
    updateActiveTab: (tab) => {
      set(() => ({
        activeTab: tab,
      }));
    },
    updateTab: (field, value) => {
      set((state) => {
        state.newTabs[state.activeTab][field] = value;
      });
    },
    runQuery: () => {
      set((state) => {
        state.newTabs[state.activeTab].isRunning = true;
      });
    },
    runQueryComplete: (tabId, results, totalResults) => {
      set((state) => {
        state.newTabs[tabId].isRunning = false;
        state.newTabs[tabId].results = results;
        state.newTabs[tabId].totalResults = totalResults;
      });
    },
  }))
);

window.store = useTabsStore;
