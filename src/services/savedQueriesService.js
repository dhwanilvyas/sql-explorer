/**
 * Saved queries should ideally communicate to an api to store adta
 */

const LOCAL_STORAGE_PREFIX = '@DataExplorer'
const SAVED_QUERIES = [
  {
    id: "1",
    query: "SELECT * FROM CUSTOMERS;",
  },
  {
    id: "2",
    query: "SELECT * FROM CATEGORIES;",
  },
  {
    id: "3",
    query: "SELECT * FROM PRODUCTS;",
  },
  {
    id: "4",
    query: "SELECT * FROM EMPLOYEES;",
  },
  {
    id: "5",
    query: "SELECT * FROM CATEGORIES WHERE ID=1;",
  },
];

(() => {
const fromLocalStorage = localStorage.getItem(
  `${LOCAL_STORAGE_PREFIX}-SavedQueries`
);
if (!fromLocalStorage) {
  localStorage.setItem(
    `${LOCAL_STORAGE_PREFIX}-SavedQueries`,
    JSON.stringify(SAVED_QUERIES)
  );
}
})();

export const getSavedQueries =() => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const fromLocalStorage = localStorage.getItem(`${LOCAL_STORAGE_PREFIX}-SavedQueries`);
      resolve(JSON.parse(fromLocalStorage));
    }, 200)
  })
}

export const getQuery = async (id) => {
  const queries = await getSavedQueries();
  const query = queries.find(q => q.id === id);
  return query?.query || null;
}

export const saveQuery = async (query) => {
  return new Promise(async (resolve, reject) => {
    let queries = await getSavedQueries();
    const lastQueryId = +queries[queries.length - 1]?.id;
    queries.push({
      id: (lastQueryId + 1).toString(),
      query,
    });

    localStorage.setItem(
      `${LOCAL_STORAGE_PREFIX}-SavedQueries`,
      JSON.stringify(queries)
    );

    resolve(lastQueryId.toString());
  });
};

export const updateQuery = (query, id) => {
  return new Promise(async (resolve, reject) => {
    let queries = await getSavedQueries();
    queries = queries.map(savedQuery => {
      if (savedQuery.id === id) {
        return {
          id,
          query,
        };
      }

      return savedQuery;
    })

    localStorage.setItem(
      `${LOCAL_STORAGE_PREFIX}-SavedQueries`,
      JSON.stringify(queries)
    );

    resolve();
  });
}

export const deleteSavedQueries = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      localStorage.removeItem(`${LOCAL_STORAGE_PREFIX}-SavedQueries`);

      resolve();
    }, 500);
  });
}

export const deleteSavedQuery = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(async () => {
      let queries = await getSavedQueries();
      if (queries.length === 1) {
        localStorage.removeItem(`${LOCAL_STORAGE_PREFIX}-SavedQueries`);
      } else {
        queries = queries.filter(query => query.id !== id);
        localStorage.setItem(
          `${LOCAL_STORAGE_PREFIX}-SavedQueries`,
          JSON.stringify(queries)
        );
      }

      resolve();
    }, 500);
  });
};