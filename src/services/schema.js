export const getSchemaList = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(["Categories", "Products", "Customers", "Employees"]);
    }, [1000]);
  })
}

export const getSchemaColumns = (schemaName) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const schema = require(`../../schemas/${schemaName.toLowerCase()}.json`);
      resolve(Object.keys(schema[0]));
    }, [1000]);
  });
}