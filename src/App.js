import React, { Suspense, lazy } from 'react';
import { NextUIProvider } from "@nextui-org/react";
import { Route, Routes, useNavigate } from "react-router-dom";
import { ErrorBoundary } from 'react-error-boundary';

const Home = lazy(() => import("./pages/home/Home"));
const Explorer = lazy(() => import("./pages/explorer/Explorer"));

function App() {
  const navigate = useNavigate();

  React.useEffect(() => {
    const loader = document.getElementById("loader");
    if (loader) {
      loader.remove();
    }
  }, [])

  return (
    <Suspense
      fallback={
        <p className="h-full flex justify-center items-center">Loading</p>
      }
    >
      <ErrorBoundary fallback={<p>Something went wrong</p>}>
        <NextUIProvider
          navigate={navigate}
          className="bg-gray-200 h-full overflow-hidden"
        >
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/explorer" element={<Explorer />} />
            <Route path="/explorer/:id" element={<Explorer />} />
          </Routes>
        </NextUIProvider>
      </ErrorBoundary>
    </Suspense>
  );
}

export default App;
