# Data Explorer app

The application is hosted at: https://main--frolicking-genie-358584.netlify.app/

Have added a few saved queries which can opened from left sidebar.

# Tech stack
- React (scafolled using Create React App)
- NextUI library for UI components
- Tailwind for CSS
- Zustand for state management
- React router dom for routing
- @uiw/react-textarea-code-editor for sql editor input

# Notion doc
Have added thoughts on ideation, design and rough system design phases here https://available-crowberry-a8a.notion.site/Data-Explorer-55946e9c0fea4ea6acf50808bfb20791?pvs=4

# Basic overview of the app
- The app has layout where with a small sidebar taking up 10% width
- It has 2 options currently, one to see the saved queries and other to explore available schemas
- These open up as a separate popup. Could have create pages for them, or maybe increase sidebar width and show them there in tab based UI, but wanted to give most real estate to sql editor and query results, as that is the core feature of the app where user will spend most of the time
- App supports a tab based where user can create new tabs, enter their queries and press run to view results. These results are hardcoded currently.
- There are a few "Saved queries" already added which can be viewed from saved queries in the sidebar, which when run, gives hardcoded output
- User can save untitled queries, update them, delete queries
- Once run, the table shows data in paginated format, where user has option to view results of different pages, increase number of rows per page has.
- User can share saved query which opens open it up on a new tab

# Performance
Site hosted on netlify which has a distributed CDN which helps delvier assets faster.

GTMetrix
GRade: A 
Performance scrore 85%
Structure score 100%

First Contentful Paint: 0.5s
Speed Index: 2.5s
Largest Contentful Paint: 1.7s
Total Blocking Time: 0ms
Cumulative Layout Shift: 0

App uses:
- useMemo hook whereever applicable to reduce expensive computations on each rerender
- React.memo, useCallback to avoid unnecessary rerenders of components

